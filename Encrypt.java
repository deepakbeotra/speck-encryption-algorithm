/**
 *
 * @Filename Encrypt.java
 *
 * @Version $Id: Encrypt.java,v 1.0 2014/03/07 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import javax.xml.bind.DatatypeConverter;
import edu.rit.util.Hex;
import edu.rit.util.Packing;

/**
 * 
 * The class implements SPECK32/64 encryption
 * 
 * @author Deepak Mahajan
 * 
 */
public class Encrypt {

	// Sub keys for rounds
	short subKeys[] = new short[22];
	short l[] = new short[25];

	/**
	 * The function expands the key to make an array of sub keys for encryption
	 * rounds
	 * 
	 * @param inputKey
	 *            64 bit key used for encryption
	 */
	public void setKeys(byte[] inputKey) {
		// Initializations
		subKeys[0] = Packing.packShortBigEndian(inputKey, 6);
		l[0] = Packing.packShortBigEndian(inputKey, 4);
		l[1] = Packing.packShortBigEndian(inputKey, 2);
		l[2] = Packing.packShortBigEndian(inputKey, 0);

		// Key Expansion
		for (int i = 0; i < 21; i++) {
			l[i + 3] = (short) (subKeys[i]
					+ (((l[i] >>> 7) & 511) | (l[i] << 9)) ^ i);
			subKeys[i + 1] = (short) (((subKeys[i] << 2) | (subKeys[i] >>> 14) & 3) ^ l[i + 3]);
		}

	}

	/**
	 * The encryption is carried on in this function
	 * 
	 * @param inputText
	 *            32 bit block to be encrypted
	 */
	public void encrypt(byte[] inputText) {
		// Left 16 bits of input
		short x = Packing.packShortBigEndian(inputText, 0);

		// Right 16 bits of input
		short y = Packing.packShortBigEndian(inputText, 2);

		// twenty two rounds of encryption function
		for (int i = 0; i < 22; i++) {
			x = (short) (((((x >>> 7) & 511) | (x << 9)) + y) ^ subKeys[i]);
			y = (short) ((y << 2 | (y >>> 14) & 3) ^ x);
		}

		// Print solution
		System.out.println(Hex.toString(x) + "" + Hex.toString(y));
	}

	/**
	 * This is the main function
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String args[]) {
		// key and plain text are required as command line arguments
		if (args.length == 2) {
			// key
			String inputKeyString = args[0];

			// plain text
			String inputTextString = args[1];

			try {

				// plain text in byte array
				byte[] inputText = DatatypeConverter
						.parseHexBinary(inputTextString);
				
				// key in byte array
				byte[] inputKey = DatatypeConverter
						.parseHexBinary(inputKeyString);

				// size of key and plain text block tested
				if (inputText.length != 4 || inputKey.length != 8) {
					System.out
							.println("Usage Error: Give 2 values <key - 8bytes> <plaintext - 4bytes>");
				} else {
					Encrypt ed = new Encrypt();
					
					// call to function setKeys for key expansion
					ed.setKeys(inputKey);
					
					// call to function encrypt for encryption
					ed.encrypt(inputText);
				}
			} catch (Exception e) {
				System.out.println("Usage Error: Give 2 values <key - 8bytes hexadecimal> "
						+ "<plaintext - 4bytes hexadecimal>");
			}
		} else {
			System.out.println("Usage Error: Give 2 values <key> <plaintext>");
		}
	}
}

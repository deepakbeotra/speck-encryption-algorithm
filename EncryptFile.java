/**
 *
 * @Filename EncryptFile.java
 *
 * @Version $Id: EncryptFile.java,v 1.0 2014/03/07 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.bind.DatatypeConverter;
import edu.rit.util.Hex;
import edu.rit.util.Packing;

/**
 * The class uses the SPECK32/64 algorithm to encrypt a file
 * 
 * @author Deepak Mahajan
 * 
 */
public class EncryptFile {

	// subkeys to be used in encryption round
	short subKeys[] = new short[22];
	short l[] = new short[25];

	/**
	 * The function expands the key to make an array of sub keys for encryption
	 * rounds
	 * 
	 * @param inputKey
	 *            64 bit key used for encryption
	 */
	public void setKeys(byte[] inputKey) {

		// Initializations
		subKeys[0] = Packing.packShortBigEndian(inputKey, 6);
		l[0] = Packing.packShortBigEndian(inputKey, 4);
		l[1] = Packing.packShortBigEndian(inputKey, 2);
		l[2] = Packing.packShortBigEndian(inputKey, 0);
		
		// Key expansion
		for (int i = 0; i < 21; i++) {
			l[i + 3] = (short) (subKeys[i]
					+ (((l[i] >>> 7) & 511) | (l[i] << 9)) ^ i);
			subKeys[i + 1] = (short) (((subKeys[i] << 2) | (subKeys[i] >>> 14) & 3) ^ l[i + 3]);

		}

	}

	/**
	 * The encryption is carried on in this function
	 * 
	 * @param inputText
	 *            32 bit block to be encrypted
	 */
	public byte[] encryptFile(byte[] inputText) {
		
		// Left 16 bits of plain text
		short x = Packing.packShortBigEndian(inputText, 0);
		
		// Right 16 bits of plain text
		short y = Packing.packShortBigEndian(inputText, 2);

		// Encryption Rounds
		for (int i = 0; i < 22; i++) {
			x = (short) (((((x >>> 7) & 511) | (x << 9)) + y) ^ subKeys[i]);
			y = (short) ((y << 2 | (y >>> 14) & 3) ^ x);

		}
		
		// Return encrypted block as byte array
		String abc = Hex.toString(x) + "" + Hex.toString(y);
		byte[] cipherWord = DatatypeConverter.parseHexBinary(abc);
		return cipherWord;
	}

	/**
	 * This is the main function.
	 * File Manipulations are in the main function
	 * 
	 * @param args    Command Line Arguments
	 */
	public static void main(String args[]) {
		
		// Key, Plain text file and Cipher text file are required as
		// command line arguments
		if (args.length == 3) {
			
			// key in hexadecimal
			String inputKeyString = args[0];
			
			// plain text file
			File textFile = new File(args[1]);
			
			// Cipher text file
			File encryptedFile = new File(args[2]);

			try {

				// Input File Stream to use plain text file
				FileInputStream textFileStream = new FileInputStream(textFile);
				
				// Plain text file in byte array
				byte[] tempInputText = new byte[(int) textFile.length()];

				// Read plain text file
				textFileStream.read(tempInputText);

				// output File Stream to use cipher text file
				FileOutputStream encryptedFileStream = new FileOutputStream(
						encryptedFile);
				
				// number of bytes to be padded
				int appendBytes = 4 - (tempInputText.length % 4);

				// new array which store the bytes of file + appended bytes.
				byte[] inputText = new byte[(tempInputText.length)
						+ (appendBytes)];
				
				// Append bytes for padding
				int count = 0;
				for (int i = 0; i < tempInputText.length; i++) {
					inputText[i] = tempInputText[i];
					count++;
				}
				inputText[count] = (byte) 0x80;
				count++;
				for (int i = 0; i < appendBytes - 1; i++) {

					inputText[count] = (byte) 0x00;
					count++;
				}
				
				// new byte array generated after appending the extra bytes.
				byte[] inputKey = DatatypeConverter
						.parseHexBinary(inputKeyString);
				
				// byte array for encrypted text
				byte[] cipherText = new byte[inputText.length];
				cipherText = inputText;
				EncryptFile edFile = new EncryptFile();
				
				// call to function setKeys for key expansion
				edFile.setKeys(inputKey);
				
				// Each block encrypted one by one
				for (int i = 0; i < cipherText.length; i = i + 4) {
					byte[] word = new byte[4];
					word[0] = cipherText[i];
					word[1] = cipherText[i + 1];
					word[2] = cipherText[i + 2];
					word[3] = cipherText[i + 3];
					
					// Call to function encryptFile for encryption
					byte[] encryptedWord = edFile.encryptFile(word);
					cipherText[i] = encryptedWord[0];
					cipherText[i + 1] = encryptedWord[1];
					cipherText[i + 2] = encryptedWord[2];
					cipherText[i + 3] = encryptedWord[3];
				}
				
				// Write encrypted text to cipher text file
				encryptedFileStream.write(cipherText);
				
				// Close streams
				textFileStream.close();
				encryptedFileStream.close();

			}

			catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				System.out.println("Usage Error: File Not Found");
				System.exit(1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Usage Error: IO Exception");
				System.exit(1);
			} catch (Exception e) {
				System.out
						.println("Usage Error: Give 3 values <key - 8bytes hexadecimal>"
								+ " <plaintextfilename> <ciphertextfilename>");
				System.exit(1);
			}
		} else {
			System.out
					.println("Usage Error: Give 3 values <key> <plaintextfilename> <ciphertextfilename>");
			System.exit(1);
		}
	}
}

/**
 *
 * @Filename DecryptFile.java
 *
 * @Version $Id: DecryptFile.java,v 1.0 2014/03/07 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.DatatypeConverter;

import edu.rit.util.Hex;
import edu.rit.util.Packing;

/**
 * The class uses the SPECK32/64 algorithm to decrypt a file
 * 
 * @author Deepak Mahajan
 * 
 */
public class DecryptFile {

	// subkeys to be used in encryption round
	short subKeys[] = new short[22];
	short l[] = new short[25];

	/**
	 * The function expands the key to make an array of sub keys for decryption
	 * rounds
	 * 
	 * @param inputKey
	 *            64 bit key used for encryption
	 */
	public void setKeys(byte[] inputKey) {

		// Initializations
		subKeys[0] = Packing.packShortBigEndian(inputKey, 6);
		l[0] = Packing.packShortBigEndian(inputKey, 4);
		l[1] = Packing.packShortBigEndian(inputKey, 2);
		l[2] = Packing.packShortBigEndian(inputKey, 0);

		// Key expansion
		for (int i = 0; i < 21; i++) {
			l[i + 3] = (short) (subKeys[i]
					+ (((l[i] >>> 7) & 511) | (l[i] << 9)) ^ i);
			subKeys[i + 1] = (short) (((subKeys[i] << 2) | (subKeys[i] >>> 14) & 3) ^ l[i + 3]);
		}
	}

	/**
	 * The decryption is carried on in this function
	 * 
	 * @param inputText
	 *            32 bit block to be encrypted
	 */
	public byte[] decryptFile(byte[] inputText) {

		// Left 16 bits of plain text
		short x = Packing.packShortBigEndian(inputText, 0);

		// Right 16 bits of plain text
		short y = Packing.packShortBigEndian(inputText, 2);

		// Decryption rounds
		for (int i = 21; i >= 0; i--) {
			y = (short) (y ^ x);
			y = (short) (((y >>> 2) & 16383) | y << 14);

			x = (short) (x ^ subKeys[i]);
			x = (short) (x - y);
			x = (short) ((x << 7) | ((x >>> 9) & 127));
		}

		// Return decrypted block as byte array
		String abc = Hex.toString(x) + "" + Hex.toString(y);
		byte[] plainWord = DatatypeConverter.parseHexBinary(abc);
		return plainWord;

	}

	/**
	 * This is the main function. File Manipulations are in the main function
	 * 
	 * @param args
	 *            Command Line Arguments
	 */
	public static void main(String args[]) {

		// Key, Plain text file and Cipher text file are required as
		// command line arguments
		if (args.length == 3) {

			// Key in hexadecimal
			String inputKeyString = args[0];

			// cipher text file
			File encryptedFile = new File(args[1]);

			// plain text file
			File textFile = new File(args[2]);
			// Input File Stream to use cipher text file
			FileInputStream encryptedFileStream;
			// Output File stream to use plain text file
			FileOutputStream textFileStream;

			try {

				 encryptedFileStream = new FileInputStream(encryptedFile);
				 textFileStream = new FileOutputStream(textFile);

				// cipher text file in byte array
				byte[] cipherText = new byte[(int) encryptedFile.length()];

				// read the cipher text file
				encryptedFileStream.read(cipherText);

				// key as byte array
				byte[] inputKey = DatatypeConverter
						.parseHexBinary(inputKeyString);

				// byte array for decrypted text
				byte[] tempPlainText = new byte[cipherText.length];
				tempPlainText = cipherText;
				DecryptFile dFile = new DecryptFile();

				// call to setKeys function for key expansion
				dFile.setKeys(inputKey);

				// Decrypting each block one by one
				for (int i = 0; i < tempPlainText.length; i = i + 4) {
					byte[] word = new byte[4];
					word[0] = tempPlainText[i];
					word[1] = tempPlainText[i + 1];
					word[2] = tempPlainText[i + 2];
					word[3] = tempPlainText[i + 3];

					byte[] decryptedWord = new byte[4];

					// call to decryptFile function for decryption
					decryptedWord = dFile.decryptFile(word);
					tempPlainText[i] = decryptedWord[0];
					tempPlainText[i + 1] = decryptedWord[1];
					tempPlainText[i + 2] = decryptedWord[2];
					tempPlainText[i + 3] = decryptedWord[3];
				}

				// Finding padding in the decrypted byte array
				// and removing it
				int count = 0;
				boolean flag = false;
				for (int i = tempPlainText.length - 1; i >= tempPlainText.length - 4; i--) {
					count++;
					if (tempPlainText[i] == (byte) 0x80) {
						flag = true;
						break;
					}
				}
				for (int i = tempPlainText.length - 1; i >= tempPlainText.length - (count-1); i--) {
					if(tempPlainText[i] != (byte) 0x00){
						flag=false;
						break;
					}
				
					
				}
				if (flag == true) {

					// Final decrypted byte array of plain text without padding
					byte[] plainText = new byte[tempPlainText.length - count];
					for (int i = 0; i < plainText.length; i++) {
						plainText[i] = tempPlainText[i];
					}
					
					// Write decrypted text to file
					textFileStream.write(plainText);

				} else {
					encryptedFileStream.close();
					textFileStream.close();
					System.out.println("Usage Error: Illegal Padding error");
					System.exit(1);
				}
				encryptedFileStream.close();
				textFileStream.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				System.out.println("Usage Error: File Not Found");
				
				System.exit(1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Usage Error:IO Exception");
				
				System.exit(1);
			} catch (Exception e) {
				System.out
						.println("Usage Error: Give 3 values <key - 8bytes hexadecimal>"
								+ " <ciphertextfilename - combination of 4 bytes)> <plaintextfilename>");
				System.exit(1);
				
			}

		} else {
			System.out
					.println("Usage Error: Give 3 values <key> <ciphertextfilename> <plaintextfilename>");
			System.exit(1);
		}
	}
}

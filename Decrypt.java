/**
 *
 * @Filename Decrypt.java
 *
 * @Version $Id: Decrypt.java,v 1.0 2014/03/07 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import javax.xml.bind.DatatypeConverter;
import edu.rit.util.Hex;
import edu.rit.util.Packing;

/**
 * This class implements SPECK32/64 decryption algorithm
 * 
 * @author Deepak Mahajan
 * 
 */
public class Decrypt {

	// Sub keys for encryption rounds
	short subKeys[] = new short[22];
	short l[] = new short[25];

	/**
	 * The function expands the encryption key
	 * for sub keys to be used in decryption rounds
	 * 
	 * @param inputKey key used for decryption
	 */
	public void setKeys(byte[] inputKey) {
		
		// Initializations
		subKeys[0] = Packing.packShortBigEndian(inputKey, 6);
		l[0] = Packing.packShortBigEndian(inputKey, 4);
		l[1] = Packing.packShortBigEndian(inputKey, 2);
		l[2] = Packing.packShortBigEndian(inputKey, 0);
		
		// Key expansion
		for (int i = 0; i < 21; i++) {
			l[i + 3] = (short) (subKeys[i]
					+ (((l[i] >>> 7) & 511) | (l[i] << 9)) ^ i);
			subKeys[i + 1] = (short) (((subKeys[i] << 2) | (subKeys[i] >>> 14) & 3) ^ l[i + 3]);
		}
	}

	/**
	 * The method has the actual decryption functionality
	 * 
	 * @param inputText    cipher input block
	 */
	public void decrypt(byte[] inputText) {
		
		// Left most 16 bits of cipher
		short x = Packing.packShortBigEndian(inputText, 0);
		
		// Right most 16 bits of cipher
		short y = Packing.packShortBigEndian(inputText, 2);
		
		// Decryption rounds
		for (int i = 21; i >= 0; i--) {
			y = (short) (y ^ x);
			y = (short) (((y >>> 2) & 16383) | y << 14);
			x = (short) (x ^ subKeys[i]);
			x = (short) (x - y);
			x = (short) ((x << 7) | ((x >>> 9) & 127));
		}
		
		// Printing solution
		System.out.println(Hex.toString(x) + "" + Hex.toString(y));

	}

	/**
	 * This is the main function
	 * 
	 * @param args  Command Line Arguments
	 */
	public static void main(String args[]) {
		
		// Key and cipher are required as command line arguments
		if (args.length == 2) {
			
			// Key
			String inputKeyString = args[0];
			
			// Cipher Text
			String inputTextString = args[1];

			try {
				
				// Cipher as byte array
				byte[] inputText = DatatypeConverter
						.parseHexBinary(inputTextString);
				
				// key as byte array
				byte[] inputKey = DatatypeConverter
						.parseHexBinary(inputKeyString);

				// Size of key and cipher are tested
				if (inputText.length != 4 || inputKey.length != 8) {
					System.out
							.println("Usage Error: Give 2 values <key - 8bytes> <ciphertext - 4bytes>");
				} else {
					Decrypt ed = new Decrypt();
					
					// call to function set keys to expand keys
					ed.setKeys(inputKey);
					
					// call to function decrypt for decryption
					ed.decrypt(inputText);
				}
			} catch (Exception e) {
				System.out
						.println("Usage Error: Give 2 values <key - 8bytes hexadecimal> <ciphertext - 4bytes hexadecimal>");
			}
		} else {
			System.out.println("Usage Error: Give 2 values <key> <ciphertext>");
		}
	}
}
